// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyBI7ESrj6ouksH2HuOjXi-v_k_p1PnV-L0',
    authDomain: 'testapidisk-205919.firebaseapp.com',
    databaseURL: 'https://testapidisk-205919.firebaseio.com',
    projectId: 'testapidisk-205919',
    storageBucket: 'testapidisk-205919.appspot.com',
    messagingSenderId: '638198439390',
    appId: '1:638198439390:web:75754cbf3d8ef64d1e114f'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
