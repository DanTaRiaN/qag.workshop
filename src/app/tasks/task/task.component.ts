import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ITask } from './../task.model';
@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
})
export class TaskComponent implements OnInit {
  @Input() task: ITask;
  @Output()
  changeStateEvent = new EventEmitter<ITask>();
  @Output()
  openTaskEvent = new EventEmitter<ITask>();
  constructor() { }
  ngOnInit() {}
  open() {
    this.openTaskEvent.emit(this.task);
  }
  complied() {
    this.changeStateEvent.emit(this.task);
  }
}
