import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { ModalController, NavParams } from '@ionic/angular';
import { ITask } from '../task.model';
@Component({
  selector: 'app-task-modal',
  templateUrl: './task-modal.component.html',
  styleUrls: ['./task-modal.component.scss']
})
export class TaskModalComponent implements OnInit {
  task: ITask = {
    name: '',
    description: '',
    image_url: '',
    data_public: '',
    state: false
  };
  taskForm: FormGroup;
  constructor(
    private modalController: ModalController,
    private params: NavParams
  ) {}
  ngOnInit() {
    this.task = this.params.data.task;
    this.taskForm = new FormGroup({
      name: new FormControl(this.task.name, [
        Validators.required,
        Validators.minLength(3)
      ]),
      description: new FormControl(this.task.description),
      image_url: new FormControl(this.task.image_url)
    });
  }
  submit() {
    const task = this.taskForm.value;
    const dateOld = this.task.data_public;
    task.data_public = dateOld !== '' ? dateOld : + new Date();
    this.dismiss(task);
  }

  dismiss(task: ITask) {
    this.modalController.dismiss(task);
  }
}
