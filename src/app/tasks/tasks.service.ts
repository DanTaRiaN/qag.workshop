import { Injectable } from '@angular/core';

import { ITask } from './task.model';

@Injectable({
  providedIn: 'root'
})
export class TasksService {
  list: ITask[] = [
    {
      name: 'testing',
      description: 'desc',
      image_url:
        'https://images.pexels.com/photos/67636/rose-blue-flower-rose-blooms-67636.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      data_public: '1252521522152',
      state: false
    },
    {
      name: 'testing',
      description: 'desc',
      image_url: '',
      data_public: '1252521522152',
      state: false
    }
  ];
  constructor() {}
  getAll() {
    return this.list;
  }
  put(task: ITask) {
    console.log(task);
    this.list.push(task);
  }
  remove(task: ITask) {
    console.log(task);
    const index = this.list.indexOf(task, 0);
    if (index > -1) {
      this.list.splice(index, 1);
    }
  }
}
