export interface ITask {
  name: string;
  description: string;
  image_url: string;
  data_public: string;
  state: boolean;
}
