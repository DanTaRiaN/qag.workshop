import { Component, OnInit } from '@angular/core';

import { ModalController } from '@ionic/angular';

import { TaskModalComponent } from './task-modal/task-modal.component';
import { ITask } from './task.model';
import { TasksService } from './tasks.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {
  tasks: ITask[];
  constructor(
    private service: TasksService,
    private modalController: ModalController
  ) {}
  ngOnInit() {
    this.tasks = this.service.getAll();
  }
  async presentModal(
    task: ITask = {
      name: '',
      description: '',
      image_url: '',
      data_public: '',
      state: false
    }
  ) {
    const modal = await this.modalController.create({
      component: TaskModalComponent,
      componentProps: {
        task
      }
    });
    const dismiss = modal.onDidDismiss();
    dismiss.then((callBackData: any) => {
      if (callBackData.data == null) {
        return;
      }
      this.service.remove(task);
      this.service.put(callBackData.data);
    });
    return await modal.present();
  }
  remove(task: ITask) {
    this.service.remove(task);
  }
  
}
