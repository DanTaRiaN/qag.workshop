import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {
  constructor(private fireauth: AngularFireAuth) {}

  ngOnInit() {}

  // signup() {
  //   this.fireauth.auth.createUserWithEmailAndPassword(this.email, this.password)
  //     .then(res => {
  //       if (res.user) {
  //         console.log(res.user);
  //       }
  //     })
  //     .catch(err => {
  //       console.log(`signup failed ${err}`);
  //       this.error = err.message;
  //     });
  // }

  // login() {
  //   this.fireauth.auth.signInWithEmailAndPassword(this.email, this.password)
  //     .then(res => {
  //       if (res.user) {
  //         console.log(res.user);
  //         this.router.navigate(['/home']);
  //       }
  //     })
  //     .catch(err => {
  //       console.log(`login failed ${err}`);
  //       this.error = err.message;
  //     });
  // }
}
