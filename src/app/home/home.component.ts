import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  news: {name:string, desc:string, image_url:string}[] = [
    {
      name: 'lesson1',
      desc: 'create project, generate Tasks and imp List View',
      image_url: 'https://www.berniemitchell.ca/wp-content/uploads/2016/04/lesson1.png'
    },
    {
      name: 'lesson2',
      desc: 'generate Task, working from Input, Service and Routing',
      image_url: 'https://www.berniemitchell.ca/wp-content/uploads/2016/04/lesson2.png'
    },
    {
      name: 'lesson3',
      desc: 'working firebase and plugin for ionic',
      image_url: 'https://www.berniemitchell.ca/wp-content/uploads/2016/04/lesson3.png'
    },
  ];
  slideOpts = {
    initialSlide: 0,
    speed: 400
  };
  constructor() { }

  ngOnInit() {}

}
