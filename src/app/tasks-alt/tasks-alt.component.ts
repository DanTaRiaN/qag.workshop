import { TasksService } from './../tasks/tasks.service';
import { ITask } from './../tasks/task.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tasks-alt',
  templateUrl: './tasks-alt.component.html',
  styleUrls: ['./tasks-alt.component.scss']
})
export class TasksAltComponent implements OnInit {
  items: ITask[] = [];
  tmpItem: ITask = {
    name: 'item1',
    description: 'description one number',
    image_url: '',
    data_public: '',
    state: false
  };

  constructor(private service: TasksService) {}

  ngOnInit() {
    this.items = this.service.getAll();
  }

  put() {
    this.tmpItem.data_public = new Date().toString();
    this.service.put(this.tmpItem);
    this.tmpItem = {
      name: 'item1',
      description: 'description one number',
      image_url: '',
      data_public: '',
      state: false
    };
  }
}
