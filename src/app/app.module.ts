import { TasksAltComponent } from './tasks-alt/tasks-alt.component';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { TasksComponent } from './tasks/tasks.component';
import { TaskComponent } from './tasks/task/task.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { TaskModalComponent } from './tasks/task-modal/task-modal.component';
import { HomeComponent } from './home/home.component';
import { HighlightDirective } from './highlight/highlight.directive';
import { AuthenticationComponent } from './authentication/authentication.component';


import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TasksComponent,
    TaskComponent,
    AboutUsComponent,
    TaskModalComponent,
    HighlightDirective,
    AuthenticationComponent,
    TasksAltComponent
  ],
  entryComponents: [
    TaskModalComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    // AngularFireModule.initializeApp(environment.firebaseConfig),
    // AngularFireAuthModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
